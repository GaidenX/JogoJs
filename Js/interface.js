/*global Phaser*/
var telaInicialKey = {};

var telaInicial = new Phaser.State();
var telaGame =  new Phaser.State();
var game = new Phaser.Game(1000, 600, Phaser.AUTO, 'phaser-example', telaInicial);
// criar StateManager e o estado inicial do game

telaInicial.preload = function(){
    telaInicialKey.key = "inicio";
    game.state.add("inicio", telaGame); 
    //game.state.add("inicio", telaInicial);

    game.load.image('background', 'image/background.jpg');
    
    // UI Menu
    game.load.image('logo','image/Logo.png');
    game.load.image('button1', 'image/buttonMusic1.png');
    game.load.image('button2', 'image/buttonMusic2.png');
    game.load.image('button3', 'image/buttonMusic3.png');
    game.load.image('button4', 'image/buttonMusic4.png');
    game.load.image('play', 'image/playButton.png');
    
    // JSON Musics
    game.load.text('version', 'js/musics.json');
    
    //AUDIOS
    // game.load.audio('initial', [ phaserJSON.squit.version]);
    game.load.audio('initial', ['audio/initial-SwingParty.mp3']);  
    game.load.audio('sound1', ['audio/sound1.mp3']);
    game.load.audio('sound2', ['audio/sound2.mp3']);
    game.load.audio('sound3', ['audio/sound3.mp3']);
}

// ------ Variaveis -------
    var backgroundVar;
    var logo;
    var button = []; // array dos botoes de musica
    var playButton;
    var music = [];
    var selMusic;
    var count = 5;
    var phaserJSON;

    var logoScale = 1;
    var limitScale = false;
    var tween;

telaInicial.create = function(){
    // get json musics
    phaserJSON = JSON.parse(game.cache.getText('version'));
    
    // ---- Playlist ----
    music[0] = game.add.audio(phaserJSON.initial.key);
    music[1] = game.add.audio(phaserJSON.music1.key);
    music[2] = game.add.audio(phaserJSON.music2.key);
    music[3] = game.add.audio(phaserJSON.music3.key);
    music[0].play("",0,1,true);
    
    
    game.stage.backgroundColor = "#8A2BE2";
    backgroundVar = game.add.sprite(0,game.world.height-600, 'background');
    backgroundVar.scale.setTo(2,1.8);
    
    // ---- UI ---
    logo = game.add.sprite(225, 275, 'logo');
    logo.anchor.setTo(0.5, 0.5);
    button[0] = game.add.sprite(670 , 25, 'button1');
    button[1] = game.add.sprite(670, 130, 'button2');
    button[2] = game.add.sprite(670, 235, 'button3');
    button[3] = game.add.sprite(670, 340, 'button4');
    game.physics.arcade.enable(button[0], Phaser.Physics.ARCADE);
    game.physics.arcade.enable(button[1], Phaser.Physics.ARCADE);
    game.physics.arcade.enable(button[2], Phaser.Physics.ARCADE);
    game.physics.arcade.enable(button[3], Phaser.Physics.ARCADE);
    playButton = game.add.sprite(825, 520, 'play');
    playButton.anchor.setTo(0.5, 0.5);
    
    credits = game.add.text(game.world.centerX - 350, 580, "Produzido por Caio =D", { font: "13px Arial Black", fill: "#ffffff" });
}

telaInicial.update = function(){
     
    for(i = 0; i < JSON.stringify(button.length); i++){
        button[i].inputEnabled = true;
        //Eventos Menu
        button[i].events.onInputOver.add(over, {tweenButton: i}, this);
        button[i].events.onInputOut.add(out, {tweenButton: i}, this);
        
        if(button[i].x <= 655){
            button[i].body.velocity.x = 0;
            button[i].x = 655;
        }
    }
    button[0].events.onInputUp.add(up, {music1: 0});
    button[1].events.onInputUp.add(up, {music1: 1});
    button[2].events.onInputUp.add(up, {music1: 2});
    button[3].events.onInputUp.add(up, {music1: 3});
    
    playButton.inputEnabled = true;
    playButton.events.onInputOver.add(overPlay, this);
    playButton.events.onInputOut.add(outPlay, this);
    playButton.events.onInputUp.add(upNewGame, this);
    
    if(logoScale < 1.1 && !limitScale){
        logoScale += 0.002;
        logo.scale.setTo(logoScale,logoScale);
        if(logoScale >= 1.1){
            limitScale = true;
        }
    }
    if(limitScale){
        logoScale -= 0.002;
        logo.scale.setTo(logoScale,logoScale);
            if(logoScale <= 1){
                limitScale = false;
            }
    }
}

function over(){
    button[this.tweenButton].game.canvas.style.cursor= "pointer";
    button[this.tweenButton].body.velocity.x -= 3;
}

function overPlay(){
    this.game.canvas.style.cursor= "pointer";
}

function out(){
     button[this.tweenButton].game.canvas.style.cursor = "default";
        button[this.tweenButton].x = 670;
        button[this.tweenButton].body.velocity.x = 0;
}

function outPlay(){
    this.game.canvas.style.cursor= "default";
}

function up(){
    selMusic = this.music1;
    console.log("selMusic: " + selMusic);
    var len = JSON.stringify(music.length);
    for(i = 0; i < len; i++){
         music[i].stop();
    }
    
    // mudanca de musica
    switch (selMusic){
    case 0: 
     music[0].play("",0,1,true);
    break;
    case 1: 
     music[1].play("",0,1,true);
    break; 
    case 2: 
     music[2].play("",0,1,true);
    break;
    case 3: 
     music[3].play("",0,1,true);
    break;
    }
}

function upNewGame(){
    game.state.start("inicio", telaGame);
}

//--- FIM -----------------------------------------------------------------

// --- Game Screen ---
// array dos botoes de musica
 var direcionalGameLeft = [];
 var direcionalGameUp = [];
 var direcionalGameBottom = []; 
 var direcionalGameRight = []; 
 var direcionalPlayer = []; //0 = Esq, 1 = Cima, 2 = baixo, 3 = direita
 var direcionalPlayerAnim = []; //0 = Esq, 1 = Cima, 2 = baixo, 3 = direita
 var animGoal = [];
 
 //Dificuldades de velocidade
 var direcionalSpeedEasy = 200;
 var direcionalSpeedMedium = 300;
 var direcionalSpeedHard = 420;

 var pontuacao = 0;
 var bonusVelocity = 60;
 var countAcertos = 0;
 var controlEvent2x = false;
 var controlEvent3x = false;
 var text1;
 var bonusText;
 

telaGame.preload = function(){   
    telaInicialKey.key = "inicio";
    game.state.add("inicio", telaInicial); 
    
    game.load.audio('initial', ['audio/initial-SwingParty.mp3']);
    game.load.audio('sound1', ['audio/sound1.mp3']);
    game.load.audio('sound2', ['audio/sound2.mp3']);
    game.load.audio('sound3', ['audio/sound3.mp3']);
    
    // Direcionais
    game.load.image('setaEsq', 'image/setaEsquerda.png');
    game.load.image('setaDir', 'image/setaDireita.png');
    game.load.image('setaCima', 'image/setaCima.png');
    game.load.image('setaBaixo', 'image/setaBaixo.png');
    
    /*
    game.load.image('setaEsqPlayer', 'image/setaPlayerEsq.png');
    game.load.image('setaDirPlayer', 'image/setaPlayerDir.png');
    game.load.image('setaCimaPlayer', 'image/setaPlayerCima.png');
    game.load.image('setaBaixoPlayer', 'image/setaPlayerBaixo.png');
    */
    
    game.load.spritesheet('setaEsqPlayerAnim', 'image/setaEsquerdaSheet.png', 75, 75, 4);
    game.load.spritesheet('setaDirPlayerAnim', 'image/setaDireitaSheet.png', 75, 75, 4);
    game.load.spritesheet('setaCimaPlayerAnim', 'image/setaCimaSheet.png', 75, 75, 4);
    game.load.spritesheet('setaBaixoPlayerAnim', 'image/setaBaixoSheet.png', 75, 75, 4);
}

telaGame.create = function(){
   switch (selMusic){
    case 0: 
     music[0].play("",0,1,false);
    break;
    case 1: 
     music[1].play("",0,1,false);
    break;
    case 2: 
     music[2].play("",0,1,false);
    break; 
    case 3: 
     music[3].play("",0,1,false);
    break; 
    }
    
    // UI IN-GAME
    text1 = game.add.text(850, 20, "Pontos: \n0", { font: "24px Arial Black", fill: "#c51b7d" });
    text1.stroke = "#de77ae";
    text1.strokeThickness = 6;
    text1.setShadow(1, 1, "#333333", 2, false, true);
    
    bonusText = game.add.text(840, 100, "Pontos: \n", { font: "24px Arial Black", fill: "#c51b7d" });
    bonusText.stroke = "#de77ae";
    bonusText.strokeThickness = 6;
    bonusText.setShadow(1, 1, "#333333", 2, false, true);
    
    direcionalPlayerAnim[0] = game.add.sprite(100, (game.height - 85), 'setaEsqPlayerAnim');
    direcionalPlayerAnim[1] = game.add.sprite(250, (game.height - 85), 'setaCimaPlayerAnim');
    direcionalPlayerAnim[2] = game.add.sprite(400, (game.height - 85), 'setaBaixoPlayerAnim');
    direcionalPlayerAnim[3] = game.add.sprite(550, (game.height - 85), 'setaDirPlayerAnim');
    
    animGoal[0] =  direcionalPlayerAnim[0].animations.add('goal');
    animGoal[1] =  direcionalPlayerAnim[1].animations.add('goal');
    animGoal[2] =  direcionalPlayerAnim[2].animations.add('goal');
    animGoal[3] =  direcionalPlayerAnim[3].animations.add('goal');
    
    // variar a posicao y dos spawns
    direcionalGameLeft[0] = game.add.sprite(100, -(Math.floor(Math.random() * 300) + 200), 'setaEsq');
    direcionalGameLeft[1] = game.add.sprite(100, -(Math.floor(Math.random() * 700) + 500), 'setaEsq');
    
    direcionalGameUp[0] = game.add.sprite(250, -(Math.floor(Math.random() * 300) + 200), 'setaCima');
    direcionalGameUp[1] = game.add.sprite(250, -(Math.floor(Math.random() * 700) + 500), 'setaCima');
    
    direcionalGameBottom[0] = game.add.sprite(400, -(Math.floor(Math.random() * 300) + 200), 'setaBaixo');
    direcionalGameBottom[1] = game.add.sprite(400, -(Math.floor(Math.random() * 700) + 500), 'setaBaixo');
    
    direcionalGameRight[0] = game.add.sprite(550, -(Math.floor(Math.random() * 300) + 200), 'setaDir');
    direcionalGameRight[1] = game.add.sprite(550, -(Math.floor(Math.random() * 700) + 500), 'setaDir');
    
    
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    for(i = 0; i < direcionalGameLeft.length; i++){
        game.physics.arcade.enable(direcionalGameLeft[i]);
        game.physics.arcade.enable(direcionalGameUp[i]);
        game.physics.arcade.enable(direcionalGameBottom[i]);
        game.physics.arcade.enable(direcionalGameRight[i]);
    }
    
    cursors = game.input.keyboard.createCursorKeys();
    
        
    // Level e suas fisicas ----------------------------------------------
    
    if(selMusic == 0){
        if(phaserJSON.initial.level == "facil"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedEasy;
            }
        }
         else if(phaserJSON.initial.level == "medio"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedMedium;
            }
        }
        else {
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedHard;
            }
        }
        
    }  
    else if (selMusic == 1){
        if(phaserJSON.music1.level == "facil"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedEasy;
            }
        }
         else if(phaserJSON.music1.level == "medio"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedMedium;
            }
        }
        else {
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedHard;
            }
        }
    }
        else if (selMusic == 2){
        if(phaserJSON.music1.level == "facil"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedEasy;
            }
        }
         else if(phaserJSON.music1.level == "medio"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedMedium;
            }
        }
        else {
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedHard;
            }
        }
    }
        else if (selMusic == 3){
        if(phaserJSON.music1.level == "facil"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedEasy;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedEasy;
            }
        }
         else if(phaserJSON.music1.level == "medio"){
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedMedium;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedMedium;
            }
        }
        else {
            for(i = 0; i < direcionalGameLeft.length; i++){
                direcionalGameLeft[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameUp[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameBottom[i].body.velocity.y = direcionalSpeedHard;
                direcionalGameRight[i].body.velocity.y = direcionalSpeedHard;
            }
        }
    }
}


telaGame.update = function(){
    text1.setText("Pontos: \n" + pontuacao);
    
    if(countAcertos >= 10 && countAcertos < 20){
        bonusText.setText(" Bonus: \n 2x");
    }   else if (countAcertos >= 20){
        bonusText.setText(" Bonus: \n 3x");
    }   else    {
        bonusText.setText(" Bonus: \n normal");
    }
       
    //mudanca de background
    if(count >= 10){
     game.time.events.add(Phaser.Timer.SECOND * 1, changeBackground, this);
        count = 0;
    }
    count += 1; 
    
    //game.input.keyboard.onUpCallback = function( e ){
        //if(e.key === "ArrowLeft"){
        if(cursors.left.isDown){
            if(direcionalGameLeft[0].y < (game.height) && direcionalGameLeft[0].y > (game.height - 100)){
                direcionalGameLeft[0].y = -(Math.floor(Math.random() * 300) + 200);
                direcionalPlayerAnim[0].animations.play('goal', 20, false);
                    if(countAcertos >= 10 && countAcertos < 20){
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                    }
                countAcertos += 1;
            } 
            if(direcionalGameLeft[1].y < (game.height) && direcionalGameLeft[1].y > (game.height - 100)){
                    direcionalGameLeft[1].y = -(Math.floor(Math.random() * 700) + 500);
                    direcionalPlayerAnim[0].animations.play('goal', 20, false);
                        if(countAcertos >= 10 && countAcertos < 20){
                            pontuacao += 2;
                        }   else if(countAcertos >= 20){
                            pontuacao += 3;
                        }   else    {
                            pontuacao += 1;
                        }
                    countAcertos += 1;
            }
        }
        
        //if(e.key === "ArrowUp"){
        if(cursors.up.isDown){
            if(direcionalGameUp[0].y < (game.height) && direcionalGameUp[0].y > (game.height - 100)){
                direcionalGameUp[0].y = -(Math.floor(Math.random() * 300) + 200);
                direcionalPlayerAnim[1].animations.play('goal', 20, false);
                    if(countAcertos >= 10 && countAcertos < 20){
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                    }
                countAcertos += 1;
            } 
            if(direcionalGameUp[1].y < (game.height) && direcionalGameUp[1].y > (game.height - 100)){
                direcionalGameUp[1].y = -(Math.floor(Math.random() * 700) + 500);
                direcionalPlayerAnim[1].animations.play('goal', 20, false);
                    if(countAcertos >= 10 && countAcertos < 20){
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                  }
                countAcertos += 1;
            }
        }
        
        //if(e.key === "ArrowDown"){
        if(cursors.down.isDown){
            if(direcionalGameBottom[0].y < (game.height) && direcionalGameBottom[0].y > (game.height - 100)){
                direcionalGameBottom[0].y = -(Math.floor(Math.random() * 300) + 200);
                direcionalPlayerAnim[2].animations.play('goal', 20, false);
                    if(countAcertos >= 10 && countAcertos < 20){
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                    }
                countAcertos += 1;
            } 
            if(direcionalGameBottom[1].y < (game.height) && direcionalGameBottom[1].y > (game.height - 100)){
                direcionalGameBottom[1].y = -(Math.floor(Math.random() * 700) + 500);
                direcionalPlayerAnim[2].animations.play('goal', 20, false);
                    if(countAcertos >= 10 && countAcertos < 20){  
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                    }
                countAcertos += 1;
            }
        }
        
        //if(e.key === "ArrowRight"){
        if(cursors.right.isDown){
            if(direcionalGameRight[0].y < (game.height) && direcionalGameRight[0].y > (game.height - 100)){
                direcionalGameRight[0].y = -(Math.floor(Math.random() * 300) + 200);
                direcionalPlayerAnim[3].animations.play('goal', 30, false);
                    if(countAcertos >= 10 && countAcertos < 20){
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                    }
                countAcertos += 1;
            } 
            if(direcionalGameRight[1].y < (game.height) && direcionalGameRight[1].y > (game.height - 100)){
                direcionalGameRight[1].y = -(Math.floor(Math.random() * 700) + 500);
                direcionalPlayerAnim[3].animations.play('goal', 30, false);
                    if(countAcertos >= 10 && countAcertos < 20){
                        pontuacao += 2;
                    }   else if(countAcertos >= 20){
                        pontuacao += 3;
                    }   else    {
                        pontuacao += 1;
                    }
                countAcertos += 1;
            }
        }
    //}
    
    // Spawns Direcionais
        //Esquerda
    if(direcionalGameLeft[0].y > game.height){
        direcionalGameLeft[0].y = -(Math.floor(Math.random() * 300) + 200);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
        else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
     if(direcionalGameLeft[1].y > game.height){
        direcionalGameLeft[1].y = -(Math.floor(Math.random() * 700) + 500);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
        //Cima
    if(direcionalGameUp[0].y > game.height){
        direcionalGameUp[0].y =  -(Math.floor(Math.random() * 300) + 200);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
     if(direcionalGameUp[1].y > game.height){
        direcionalGameUp[1].y = -(Math.floor(Math.random() * 700) + 500);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
        //Baixo
    if(direcionalGameBottom[0].y > game.height){
        direcionalGameBottom[0].y =  -(Math.floor(Math.random() * 300) + 200);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
     if(direcionalGameBottom[1].y > game.height){
        direcionalGameBottom[1].y = -(Math.floor(Math.random() * 700) + 500);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
        //Direita
    if(direcionalGameRight[0].y > game.height){
        direcionalGameRight[0].y = -(Math.floor(Math.random() * 300) + 200);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
     if(direcionalGameRight[1].y > game.height){
        direcionalGameRight[1].y = -(Math.floor(Math.random() * 700) + 500);
        countAcertos = 0;
            if(controlEvent3x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameUp[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameBottom[i].body.velocity.y -= (bonusVelocity + 70);
                    direcionalGameRight[i].body.velocity.y -= (bonusVelocity + 70);
            }
        }
            else if (controlEvent2x){
                for(i = 0; i < direcionalGameLeft.length; i++){
                    direcionalGameLeft[i].body.velocity.y -= bonusVelocity;
                    direcionalGameUp[i].body.velocity.y -= bonusVelocity;
                    direcionalGameBottom[i].body.velocity.y -= bonusVelocity;
                    direcionalGameRight[i].body.velocity.y -= bonusVelocity;
            }
        }
        controlEvent2x = false;
        controlEvent3x = false;
    }
    
    // Bonus de velocidade e pontos
    
    //if(pontuacao == 10 && controlEvent == true){
    if(countAcertos == 10 && controlEvent2x == false){
        bonusRound(2);
    }
    if(countAcertos == 20 && controlEvent3x == false){
        bonusRound(3);
    }
    
    music[0].onStop.add(soundStopped, this);
    music[1].onStop.add(soundStopped, this);
    music[2].onStop.add(soundStopped, this);
    music[3].onStop.add(soundStopped, this);
}

function changeBackground(){
    var c = Phaser.Color.getRandomColor(50, 255, 255);
    game.stage.backgroundColor = c;
}

function bonusRound(bonus){
    switch (bonus){
    case 2: 
    for(i = 0; i < direcionalGameLeft.length; i++){
        direcionalGameLeft[i].body.velocity.y += bonusVelocity;
        direcionalGameUp[i].body.velocity.y += bonusVelocity;
        direcionalGameBottom[i].body.velocity.y += bonusVelocity;
        direcionalGameRight[i].body.velocity.y += bonusVelocity;
    }
            controlEvent2x = true;
    break;
    case 3: 
    for(i = 0; i < direcionalGameLeft.length; i++){
        direcionalGameLeft[i].body.velocity.y += (bonusVelocity + 70);
        direcionalGameUp[i].body.velocity.y += (bonusVelocity + 70);
        direcionalGameBottom[i].body.velocity.y += (bonusVelocity + 70);
        direcionalGameRight[i].body.velocity.y += (bonusVelocity + 70);
    }
            controlEvent3x = true;
    break;
    }
   // controlEvent = false;
}

function soundStopped(sound){
    for(i = 0; i < direcionalGameLeft.length; i++){
        direcionalGameLeft[i].body.velocity.y = 0;
        direcionalGameUp[i].body.velocity.y = 0;
        direcionalGameBottom[i].body.velocity.y = 0;
        direcionalGameRight[i].body.velocity.y = 0;
    }
         setTimeout(function () {
             pontuacao = 0;
             game.state.start("inicio");
             console.log("Fim de jogo");
    }, 4000);
}
